(setq inhibit-splash-screen t
      initial-scratch-message nil
      initial-major-mode 'org-mode)
;; UI configurations
(scroll-bar-mode -1)
(tool-bar-mode   -1)
(tooltip-mode    -1)
(menu-bar-mode   -1)
(global-linum-mode 1)
(add-to-list 'default-frame-alist '(font . "Iosevka-11"))
(setq make-backup-files nil)
(setq auto-save-default nil)
(defalias 'yes-or-no-p 'y-or-n-p)
(setq ring-bell-function 'ignore)

;; Show matching parens
(setq show-paren-delay 0)
(show-paren-mode  1)

;; Theme
(use-package doom-themes
  :ensure t
  :config
  (load-theme 'doom-molokai t))
;; Powerline
(use-package powerline
  :ensure t 
  :config
  (powerline-default-theme))
;; Autopair
(use-package autopair
  :ensure t
  :config
  (autopair-global-mode))
(provide 'init-gui)
