;; == company-mode ==
;(use-package company
  ;:ensure t
  ;:defer t
  ;:init (add-hook 'after-init-hook 'global-company-mode)
  ;:config
  ;(use-package company-go :ensure t :defer t)
  ;(use-package company-irony :ensure t :defer t)
  ;(use-package company-irony-c-headers :ensure t :defer t)
  ;(use-package company-tern :ensure t :defer t)

  ;(setq company-idle-delay              0.1
        ;company-minimum-prefix-length   3
        ;company-begin-commands          '(self-insert-command)
        ;company-show-numbers            t
        ;company-tooltip-limit           20
        ;company-dabbrev-downcase        nil
        ;company-echo-delay              0
        ;company-backends                '((company-elisp
                                           ;company-gtags)))

  ;:bind("<tab>" . 'company-complete-common-or-cycle))
;;; == ycmd ==
;(use-package ycmd
  ;:ensure t
  ;:init
  ;(set-variable 'ycmd-server-command
    ;'("python3" "/home/wandy/developer/ycmd/ycmd"))

  ;:config
  ;(with-eval-after-load 'company
    ;'(push 'company-ycmd company-backends))
  ;(add-hook 'after-init-hook #'global-ycmd-mode)
  ;)

;(use-package company-ycmd
  ;:ensure t
  ;:init
  ;(company-ycmd-setup)
  ;)
;; Autocomplete
(use-package auto-complete
  :ensure t
  :init 
  (progn 
    (ac-config-default)
    (global-auto-complete-mode t)
    ))
(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))
(provide 'init-autocomplete)
