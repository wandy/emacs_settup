;; Js
(use-package js2-mode
  :ensure t
  :init
  (add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))

(use-package tern :ensure t)
;; Typescript
(use-package typescript-mode
  :ensure t
  :init
  (add-to-list 'auto-mode-alist '("\\.ts\\'" . typescript-mode)))

;; yaml mode
(use-package yaml-mode
  :ensure t
  :init
  (add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode)))

;; web mode
(use-package web-mode 
  :ensure t
  :init 
  (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode)))

;; haskell mode
(use-package haskell-mode
  :ensure t
  :init
  (add-to-list 'auto-mode-alist '("\\.hs\\'" . haskell-mode)))

;; dart mode
(use-package dart-mode
  :ensure t
  :init
  (add-to-list 'auto-mode-alist '("\\.dart\\'" . haskell-mode)))
  :custom
  (dart-format-on-save t)
  (dart-enable-analysis-server t)
  (dart-sdk-path "/Applications/flutter/bin/cache/dart-sdk/"))

;; flutter
(use-package flutter
  :after dart-mode
  :bind (:map dart-mode-map
              ("C-M-x" . #'flutter-run-or-hot-reload))
  :custom
  (flutter-sdk-path "/Applications/flutter/"))

(provide 'init-leanguage)

